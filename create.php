<?php
include_once 'config.php';

use models\Contact;
use services\ContactService;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $contact = new Contact(null, $_POST["name"], $_POST["firstname"]);

    ContactService::create($contact);
    header("Location: index.php");
}
?>

<h1>Create</h1>

<form method="post">
    <label for="name">Name: </label><input name="name" id="name" type="text"/>
    <label for="firstname">Firstname: </label><input name="firstname" id="firstname" type="text"/>

    <button type="submit">Create</button>
</form>
