<?php
include_once 'config.php';

use services\ContactService;

$contact = ContactService::findById($_GET["id"]);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $contact->name = $_POST["name"];
    $contact->firstname = $_POST["firstname"];

    ContactService::update($contact);
    header("Location: index.php");
}
?>

<h1>Update</h1>

<form method="post">
    <label for="name">Name: </label><input name="name" id="name" type="text"
                                           value="<?= htmlspecialchars($contact->name) ?>"/>
    <label for="firstname">Firstname: </label><input name="firstname" id="firstname" type="text"
                                                     value="<?= htmlspecialchars($contact->firstname) ?>"/>

    <button type="submit">Update</button>
</form>
