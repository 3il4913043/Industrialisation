<?php
require_once __DIR__ . "/src/repositories/ContactRepository.php";
require_once __DIR__ . "/src/services/ContactService.php";
require_once __DIR__ . "/src/models/Contact.php";

$_ENV['DB_URL'] = "pgsql:host=localhost;port=5433;dbname=tests_project;user=admin;password=admin";