<?php

namespace integrationTests;
require_once __DIR__ . "/../../src/models/Contact.php";
require_once __DIR__ . "/../../src/repositories/ContactRepository.php";
require_once __DIR__ . "/../../src/services/ContactService.php";

$_ENV['DB_URL'] = "sqlite:tests/db_tests.sqlite";

use models\Contact;
use PDO;
use PHPUnit\Framework\TestCase;
use services\ContactService;

final class ContactServiceIntegrationTest extends TestCase
{
    private Contact $contact1;
    private Contact $contact2;
    private Contact $contact3;

    protected function setUp(): void
    {
        $this->contact1 = new Contact(1, "FETTER", "Léo");
        $this->contact2 = new Contact(2, "MARTIN", "Jérémy");
        $this->contact3 = new Contact(3, "OHIN", "Elvis");

        $connection = new PDO($_ENV['DB_URL']);
        $connection->query("INSERT INTO contact VALUES (1,'FETTER','Léo')");
        $connection->query("INSERT INTO contact VALUES (2,'MARTIN','Jérémy')");
    }

    protected function tearDown(): void
    {
        $connection = new PDO($_ENV['DB_URL']);
        $connection->query("DELETE FROM contact");
    }

    public function testDeleteAll()
    {
        ContactService::deleteAll();

        $this->assertEmpty(ContactService::findAll());
    }
}
