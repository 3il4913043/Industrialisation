<?php

namespace unitTests;
require_once __DIR__ . "/../../src/models/Contact.php";
require_once __DIR__ . "/../../src/repositories/ContactRepository.php";
require_once __DIR__ . "/../../src/services/ContactService.php";

$_ENV['DB_URL'] = "sqlite:tests/db_tests.sqlite";

use Exception;
use models\Contact;
use PDO;
use PHPUnit\Framework\TestCase;
use services\ContactService;
use TypeError;

final class ContactServiceUnitTest extends TestCase
{
    private Contact $contact1;
    private Contact $contact2;
    private Contact $contact3;

    protected function setUp(): void
    {
        $this->contact1 = new Contact(1, "FETTER", "Léo");
        $this->contact2 = new Contact(2, "MARTIN", "Jérémy");
        $this->contact3 = new Contact(3, "OHIN", "Elvis");

        $connection = new PDO($_ENV['DB_URL']);
        $connection->query("INSERT INTO contact VALUES (1,'FETTER','Léo')");
        $connection->query("INSERT INTO contact VALUES (2,'MARTIN','Jérémy')");
    }

    protected function tearDown(): void
    {
        $connection = new PDO($_ENV['DB_URL']);
        $connection->query("DELETE FROM contact");
    }

    public function testFindAll(): void
    {
        $this->assertEquals([$this->contact1, $this->contact2], ContactService::findAll());
    }

    public function testFindById(): void
    {
        $this->assertEquals($this->contact1, ContactService::findById($this->contact1->id));
        $this->assertEquals($this->contact2, ContactService::findById($this->contact2->id));
    }

    public function testFindByIdWhenIdIsNull(): void
    {
        $this->expectException(TypeError::class);

        ContactService::findById(null);
    }

    public function testFindByIdWhenIdEquals0(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Id must be greater than 0");

        ContactService::findById(0);
    }

    public function testFindByIdWhenIdIsLowerThan0(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Id must be greater than 0");

        ContactService::findById(-2);
    }

    public function testCreate(): void
    {
        ContactService::create($this->contact3);

        $this->assertEquals([$this->contact1, $this->contact2, $this->contact3], ContactService::findAll());
        $this->assertEquals($this->contact3, ContactService::findById($this->contact3->id));
    }

    public function testCreateWhenNameIsNull(): void
    {
        $this->expectException(TypeError::class);

        $contact = new Contact(10, null, "Léo");
        ContactService::create($contact);
    }

    public function testCreateWhenNameIsEmpty(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Contact name can't be empty");

        $contact = new Contact(10, "", "Léo");
        ContactService::create($contact);
    }

    public function testCreateWhenNameContainsOnlySpaces(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Contact name can't be empty");

        $contact = new Contact(10, "   ", "Léo");
        ContactService::create($contact);
    }

    public function testCreateWhenFirstnameIsNull(): void
    {
        $this->expectException(TypeError::class);

        $contact = new Contact(10, "FETTER", null);
        ContactService::create($contact);
    }

    public function testCreateWhenFirstnameIsEmpty(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Contact firstname can't be empty");

        $contact = new Contact(10, "FETTER", "");
        ContactService::create($contact);
    }

    public function testCreateWhenFirstnameContainsOnlySpaces(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Contact firstname can't be empty");

        $contact = new Contact(10, "FETTER", "   ");
        ContactService::create($contact);
    }

    public function testUpdate(): void
    {
        $this->contact2->name = "MARTINE";
        $this->contact2->firstname = "Jérémi";
        ContactService::update($this->contact2);

        $this->assertEquals([$this->contact1, $this->contact2], ContactService::findAll());
        $this->assertEquals($this->contact2, ContactService::findById($this->contact2->id));
    }

    public function testUpdateWhenNameIsNull(): void
    {
        $this->expectException(TypeError::class);

        $this->contact2->name = null;
        ContactService::update($this->contact2);
    }

    public function testUpdateWhenNameIsEmpty(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Contact name can't be empty");

        $this->contact2->name = "";
        ContactService::update($this->contact2);
    }

    public function testUpdateWhenNameContainsOnlySpaces(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Contact name can't be empty");

        $this->contact2->name = "   ";
        ContactService::update($this->contact2);
    }

    public function testUpdateWhenFirstnameIsNull(): void
    {
        $this->expectException(TypeError::class);

        $this->contact2->firstname = null;
        ContactService::update($this->contact2);
    }

    public function testUpdateWhenFirstnameIsEmpty(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Contact firstname can't be empty");

        $this->contact2->firstname = "";
        ContactService::update($this->contact2);
    }

    public function testUpdateWhenFirstnameContainsOnlySpaces(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Contact firstname can't be empty");

        $this->contact2->firstname = "   ";
        ContactService::update($this->contact2);
    }

    public function testDelete(): void
    {
        ContactService::delete($this->contact2->id);

        $this->assertEquals([$this->contact1], ContactService::findAll());
    }

    public function testDeleteWhenIdIsNull(): void
    {
        $this->expectException(TypeError::class);

        ContactService::delete(null);
    }

    public function testDeleteWhenIdEquals0(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Id must be greater than 0");

        ContactService::delete(0);
    }

    public function testDeleteWhenIdIsLowerThan0(): void
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Id must be greater than 0");

        ContactService::delete(-7);
    }
}
