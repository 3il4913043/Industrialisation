<?php

namespace repositories;

use models\Contact;
use PDO;

final class ContactRepository
{

    public static function findAll(): array
    {
        $connection = new PDO($_ENV["DB_URL"]);

        $query = "SELECT * FROM contact";
        $result = $connection->query($query);

        $contacts = [];

        foreach ($result->fetchAll() as $row) {
            $contact = new Contact($row["id"], $row["name"], $row["firstname"]);
            $contacts[] = $contact;
        }

        return $contacts;
    }

    public static function findById(int $id): Contact
    {
        $connection = new PDO($_ENV["DB_URL"]);

        $query = "SELECT * FROM contact WHERE id=$id";
        $result = $connection->query($query);
        $row = $result->fetchAll()[0];

        return new Contact($row["id"], $row["name"], $row["firstname"]);
    }

    public static function create(Contact $contact): void
    {
        $connection = new PDO($_ENV["DB_URL"]);

        $query = "INSERT INTO contact(name,firstname) VALUES ('$contact->name','$contact->firstname')";
        $connection->query($query);
    }

    public static function update(Contact $contact): void
    {
        $connection = new PDO($_ENV["DB_URL"]);

        $query = "UPDATE contact SET name='$contact->name', firstname='$contact->firstname' WHERE id=$contact->id";
        $connection->query($query);
    }

    public static function deleteAll()
    {
        $connection = new PDO($_ENV["DB_URL"]);

        $query = "DELETE FROM contact";
        $connection->query($query);
    }

    public static function delete(int $id): void
    {
        $connection = new PDO($_ENV["DB_URL"]);

        $query = "DELETE FROM contact WHERE id=$id";
        $connection->query($query);
    }
}
