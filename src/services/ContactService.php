<?php

namespace services;

use Exception;
use repositories\ContactRepository;
use models\Contact;

final class ContactService
{
    public static function findAll(): array
    {
        $contacts = ContactRepository::findAll();
        sort($contacts);
        return $contacts;
    }

    /**
     * @throws Exception
     */
    public static function findById(int $id): Contact
    {
        $exception = self::checkId($id);

        if ($exception == null) {
            return ContactRepository::findById($id);
        } else {
            throw $exception;
        }
    }

    /**
     * @throws Exception
     */
    public static function create(Contact $contact): void
    {
        $exception = self::checkContact($contact);

        if ($exception == null) {
            ContactRepository::create($contact);
        } else {
            throw $exception;
        }
    }

    /**
     * @throws Exception
     */
    public static function update(Contact $contact): void
    {
        $exception = self::checkContact($contact);

        if ($exception == null) {
            ContactRepository::update($contact);
        } else {
            throw $exception;
        }
    }

    public static function deleteAll(): void
    {
        ContactRepository::deleteAll();
    }

    /**
     * @throws Exception
     */
    public static function delete(int $id): void
    {
        $exception = self::checkId($id);

        if ($exception == null) {
            ContactRepository::delete($id);
        } else {
            throw $exception;
        }
    }

    /**
     * @param Contact $contact
     * @return Exception|null
     */
    private static function checkContact(Contact $contact): ?Exception
    {
        if ($contact->name == null && $contact->name != "") {
            return new Exception("Contact name can't be null");
        }

        if (strlen(trim($contact->name)) == 0) {
            return new Exception("Contact name can't be empty");
        }

        if ($contact->firstname == null && $contact->firstname != "") {
            return new Exception("Contact firstname can't be null");
        }

        if (strlen(trim($contact->firstname)) == 0) {
            return new Exception("Contact firstname can't be empty");
        }

        return null;
    }

    /**
     * @param int $id
     * @return Exception|null
     */
    private static function checkId(int $id): ?Exception
    {
        if ($id == null && $id != 0) {
            return new Exception("Id can't be null");
        }

        if ($id <= 0) {
            return new Exception("Id must be greater than 0");
        }

        return null;
    }
}
