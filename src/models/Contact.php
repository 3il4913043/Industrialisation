<?php

namespace models;

class Contact
{
    public int|null $id;
    public string $name;
    public string $firstname;

    public function __construct(int|null $id, string $name, string $firstName)
    {
        $this->id = $id;
        $this->name = $name;
        $this->firstname = $firstName;
    }
}
