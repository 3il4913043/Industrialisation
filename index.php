<?php
include_once 'config.php';

use services\ContactService;

$contacts = ContactService::findAll();

echo '<h1>Contacts</h1>';

echo '<a href="create.php"><button>Create</button></a>';

echo '<table>';
echo '<tr><th>ID</th><th>Name</th><th>Firstname</th><th>Actions</th></tr>';

foreach ($contacts as $contact) {
    echo '<tr>';

    echo '<td>';
    echo $contact->id;
    echo '</td>';

    echo '<td>';
    echo $contact->name;
    echo '</td>';

    echo '<td>';
    echo $contact->firstname;
    echo '</td>';

    echo '<td>';

    echo '<a href="update.php?id=';
    echo $contact->id;
    echo '"><button>Update</button></a>';

    echo '<a href="delete.php?id=';
    echo $contact->id;
    echo '"><button>Delete</button></a>';

    echo '</td>';

    echo '</tr>';
}
echo '</table>';

